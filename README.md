**Project Goal:**
Our engineering team requires a centralized documentation platform to easily search for project-related documentation, How-to (s), common pitfalls, etc. We aim to conduct a POC for hosting a static website on AWS S3, utilizing CloudFront and NLB for content distribution to various branch regions.

**Services and Components:**

**AWS S3:** for static website hosting.

**AWS CloudFront :** for faster content distribution.

**NLB (Network Load Balancer):** to balance incoming traffic and ensure availability of content to clients even during heavy loads.

**DNS (Domain Name System):** for the domain of the website

**Project Steps:**

https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-custom-domain-walkthrough.html#add-bucket-policy-root-domain

https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-cloudfront-walkthrough.html
